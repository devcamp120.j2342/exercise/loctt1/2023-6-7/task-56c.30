package com.devcamp.s10.task56c30.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import models.Book;

@Service
public class BookService {
     Book bookA = new Book("Book A", 10, 20.5);
     Book bookB = new Book("Book B", 15, 30.5);
     Book bookC = new Book("Book C", 20, 40.5);

      @Autowired
    private AuthorService authorService;
     public ArrayList<Book> allBooks() {
        bookA.setAuthors(authorService.authorsA());
        bookB.setAuthors(authorService.authorsB());
        bookC.setAuthors(authorService.authorsC());

        ArrayList<Book> books = new ArrayList<>();
        books.add(bookA);
        books.add(bookB);
        books.add(bookC);
        return books;

    }

    public ArrayList<Book> findBooksByQuatity(double qty) {
        ArrayList<Book> books = allBooks();
        ArrayList<Book> arrayResult = new ArrayList<>();

        for (Book book : books) {
            if (book.getQty() >= qty) {
                arrayResult.add(book);
            }
        }
        return arrayResult;

    }
}
