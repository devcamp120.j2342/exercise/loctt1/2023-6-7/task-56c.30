package com.devcamp.s10.task56c30.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task56c30.bookauthorapi.services.AuthorService;

import models.Author;

@RestController
@CrossOrigin
public class AuthorController {
     @Autowired
     private AuthorService authorService;

     @GetMapping("/author-info")
     public Author finAuthor(@RequestParam(required = true, name = "email")String email){
          Author author = authorService.findAuthorByEmail(email);
          return author;
     }

     @GetMapping("/author-gender")
     public ArrayList<Author> findAuthorGender(@RequestParam(required = true, name = "gender")char gender){
          ArrayList<Author> authors = authorService.findAuthorByGender(gender);
          return authors;
     }
}
