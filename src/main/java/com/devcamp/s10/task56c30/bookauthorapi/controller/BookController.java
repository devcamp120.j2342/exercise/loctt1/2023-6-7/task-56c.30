package com.devcamp.s10.task56c30.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task56c30.bookauthorapi.services.BookService;

import models.Book;

@RestController
@CrossOrigin
public class BookController {
     @Autowired
     private BookService bookService;

     @GetMapping("/books")
     public ArrayList<Book> findALlBook(){
          ArrayList<Book> books = bookService.allBooks();
          return books;
     }

     @GetMapping("/book-quantity")
     public ArrayList<Book> findBookQuantity(@RequestParam(required = true, name = "qty") double quantity){
               ArrayList<Book> books = bookService.findBooksByQuatity(quantity);
               return books;
     }
}
